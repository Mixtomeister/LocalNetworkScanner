package es.thinkback.localnetworkscanner.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import es.thinkback.localnetworkscanner.R
import es.thinkback.localnetworkscanner.scanner.LocalIpReachable

class ScanIpListAdapter(var array: ArrayList<LocalIpReachable>): RecyclerView.Adapter<ScanIpViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ScanIpViewHolder {
        return ScanIpViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.cell_ip, parent, false))
    }

    override fun getItemCount(): Int {
        return array.size
    }

    override fun onBindViewHolder(holder: ScanIpViewHolder, position: Int) {
        holder.text_ip.setText(array[position].address.hostAddress)
        holder.text_ping.setText("${array[position].ping.toString()} ms")
    }
}

class ScanIpViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
    var text_ip: TextView = itemView.findViewById(R.id.text_ip_reachable)
    var text_ping: TextView = itemView.findViewById(R.id.text_pin_ip_reachable)
}