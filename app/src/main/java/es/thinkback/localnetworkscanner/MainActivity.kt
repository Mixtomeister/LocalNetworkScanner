package es.thinkback.localnetworkscanner

import android.content.Context
import android.net.DhcpInfo
import android.net.wifi.WifiManager
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import es.thinkback.localnetworkscanner.adapters.ScanIpListAdapter
import es.thinkback.localnetworkscanner.fragment.ScanIpListFragment
import es.thinkback.localnetworkscanner.scanner.LocalIpReachable
import es.thinkback.localnetworkscanner.scanner.LocalNetworkScanner
import es.thinkback.localnetworkscanner.scanner.LocalNetworkScannerIPReachabledListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import android.os.StrictMode
import android.support.v7.widget.LinearLayoutManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import es.thinkback.localnetworkscanner.fragment.MyConfigFragment
import kotlinx.android.synthetic.main.fragment_my_config.*
import kotlinx.android.synthetic.main.fragment_scan_ip_list.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, LocalNetworkScannerIPReachabledListener, View.OnClickListener {

    lateinit var scanIpListFragment: ScanIpListFragment
    lateinit var configFragment: MyConfigFragment
    var arrayIp = ArrayList<LocalIpReachable>()
    lateinit var dhcpInfo: DhcpInfo
    lateinit var scan: LocalNetworkScanner

    override fun onCreate(savedInstanceState: Bundle?) {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        scanIpListFragment = supportFragmentManager.findFragmentById(R.id.scanfragment) as ScanIpListFragment
        configFragment = supportFragmentManager.findFragmentById(R.id.configfragment) as MyConfigFragment
        scanIpListFragment.fab.setOnClickListener(this)

        scanIpListFragment.list.layoutManager = LinearLayoutManager(applicationContext)

        var wifi = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        dhcpInfo = wifi.dhcpInfo
        openScan()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_local_network -> {
                openScan()
            }
            R.id.nav_network_config -> {
                openConfig()
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onIPReachabledFound(localIp: LocalIpReachable) {
        runOnUiThread {
            arrayIp.add(localIp)
            scanIpListFragment.list.adapter = ScanIpListAdapter(arrayIp)
            println("${localIp.address.hostAddress} - ${localIp.ping}")
        }
    }

    override fun onScanStart(numIpToScan: Int) {
        runOnUiThread {
            scanIpListFragment.progressBar.progress = 0
            fab.hide()
        }
    }

    override fun onScanUpdate(progress: Int) {
        runOnUiThread {
            scanIpListFragment.progressBar.progress = progress
        }
    }

    override fun onScanEnd(ipReachables: Int) {
        runOnUiThread {
            Toast.makeText(applicationContext, "Scan completed", Toast.LENGTH_LONG).show()
            fab.show()
        }
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.fab -> {
                scan = LocalNetworkScanner(dhcpInfo)
                scan.startScan(this, 1000)
                scanIpListFragment.progressBar.progress = 0
                arrayIp = ArrayList()
                scanIpListFragment.list.adapter = ScanIpListAdapter(arrayIp)
            }
        }
    }

    fun loadInfo(){
        config_ip.setText(parseToStringIP(Integer.toBinaryString(dhcpInfo.ipAddress)).split(".").reversed().joinToString("."))
        config_subnet.setText(parseToStringIP(fillBinary(Integer.toBinaryString(dhcpInfo.netmask), 32)))
        config_gateway.setText(parseToStringIP(Integer.toBinaryString(dhcpInfo.gateway)))
        config_dns1.setText(parseToStringIP(Integer.toBinaryString(dhcpInfo.dns1)).split(".").reversed().joinToString("."))
        config_dns2.setText(parseToStringIP(Integer.toBinaryString(dhcpInfo.dns2)).split(".").reversed().joinToString("."))
    }

    private fun parseToStringIP(binaryIp: String): String {
        var res = Integer.parseInt(binaryIp.substring(0, 8), 2).toString() + "."
        res += Integer.parseInt(binaryIp.substring(8, 16), 2).toString() + "."
        res += Integer.parseInt(binaryIp.substring(16, 24), 2).toString() + "."
        res += Integer.parseInt(binaryIp.substring(24), 2).toString()
        return res
    }

    private fun fillBinary(str: String, len: Int): String {
        var res = str
        if (str.length < len) {
            for (i in 1..(len - str.length)) {
                res = "${res}0"
            }
        }
        return res
    }

    private fun openScan(){
        val trans = supportFragmentManager.beginTransaction()
        trans.hide(configFragment)
        trans.show(scanIpListFragment)
        trans.commit()
    }

    private fun openConfig(){
        val trans = supportFragmentManager.beginTransaction()
        trans.hide(scanIpListFragment)
        trans.show(configFragment)
        trans.commit()
        loadInfo()
    }

}
