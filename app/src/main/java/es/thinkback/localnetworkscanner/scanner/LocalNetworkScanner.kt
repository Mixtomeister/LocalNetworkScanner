package es.thinkback.localnetworkscanner.scanner

import android.content.Context
import android.net.DhcpInfo
import android.net.wifi.WifiManager
import android.os.AsyncTask
import java.net.Inet4Address
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*

class LocalNetworkScanner(var dhcpInfo: DhcpInfo) : AsyncTask<Void, Int, Int>() {

    val localIP: String
    val subnetMask: Int

    private lateinit var listener: LocalNetworkScannerIPReachabledListener
    private var timeout: Int = 0
    private var iterations = 0

    /**
     * Almaceno en atributos la ip y la máscara de subred
     */
    init {
        this.localIP = parseToStringIP(Integer.toBinaryString(dhcpInfo.ipAddress)).split(".").reversed().joinToString(".")
        this.subnetMask = Integer.toBinaryString(dhcpInfo.netmask).replace("0", "").length
    }

    /**
     * Almaceno el listener al que se va a ir notificando el proceso y el tiempo máximo que
     * se va a esperar para la respuesta del nodo
     */
    fun startScan(listener: LocalNetworkScannerIPReachabledListener, timeout: Int) {
        this.listener = listener
        this.timeout = timeout
        println("$localIP/$subnetMask")
        execute()
    }

    /**
     * Método que se encarga del proceso de escaneo
     */
    override fun doInBackground(vararg params: Void?): Int {
        //paso la ip local a un string binario
        val binaryLocalIp = getBinaryString(localIP)
        //Separo en variables la parte de red y la parte de los nodos
        val netblock = binaryLocalIp.substring(0, subnetMask)
        var nodesblock = binaryLocalIp.substring(subnetMask).replace("0", "1")
        //Almaceno el número total de nodos de los que puede disponer la red
        this.iterations = Integer.parseInt(nodesblock, 2)
        var ipFounds = 0
        //Notifico de que se va a empezar a realizar el escaneo de la red
        listener.onScanStart(iterations)
        for (i in 0..iterations) {
            //parseo de binario a número la ip que con la que voy a intentar conectar
            val ip = InetAddress.getByName(parseToStringIP(netblock + fillBinary(Integer.toBinaryString(i), 8)))
            //Recojo la fecha para tener un tiempo de conexión aprox
            var timerStart = Date()
            //Intento conectar con la ip
            if (ip.isReachable(timeout)) {
                //Paro el timer y notifico que la ip es accesible
                var timerStop = Date()
                listener.onIPReachabledFound(LocalIpReachable(ip, (timerStop.time - timerStart.time).toInt()))
                ipFounds++
            }
            //Publico el progreso
            publishProgress(i)
        }
        //Devuelvo como resultado el número de ips encontradas
        return ipFounds
    }

    override fun onProgressUpdate(vararg values: Int?) {
        listener.onScanUpdate((values[0]!! * 100) / iterations)
    }

    override fun onPostExecute(result: Int?) {
        listener.onScanEnd(result!!)
    }

    /**
     * Recibe una ip xxx.xxx.xxx.xxx y la pasa a string binario
     */
    private fun getBinaryString(ip: String): String {
        var res: String = ""
        ip.split(".").forEach { block ->
            res += fillBinary(Integer.toBinaryString(block.toInt()), 8)
        }

        return res
    }

    /**
     * Recibe una ip en string binario y la pasa a xxx.xxx.xxx.xxx
     */
    private fun parseToStringIP(binaryIp: String): String {
        var res = Integer.parseInt(binaryIp.substring(0, 8), 2).toString() + "."
        res += Integer.parseInt(binaryIp.substring(8, 16), 2).toString() + "."
        res += Integer.parseInt(binaryIp.substring(16, 24), 2).toString() + "."
        res += Integer.parseInt(binaryIp.substring(24), 2).toString()
        return res
    }

    /**
     * Rellena el string binario con 0s a la izquierda
     */
    private fun fillBinary(str: String, len: Int): String {
        var res = str
        if (str.length < len) {
            for (i in 1..(len - str.length)) {
                res = "0$res"
            }
        }
        return res
    }
}

interface LocalNetworkScannerIPReachabledListener{
    fun onIPReachabledFound(localIp: LocalIpReachable)
    fun onScanStart(numIpToScan: Int)
    fun onScanUpdate(progress: Int)
    fun onScanEnd(ipReachables: Int)
}

class LocalIpReachable(val address: InetAddress, val ping: Int)