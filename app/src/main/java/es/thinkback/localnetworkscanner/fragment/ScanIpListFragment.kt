package es.thinkback.localnetworkscanner.fragment


import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar

import es.thinkback.localnetworkscanner.R


/**
 * A simple [Fragment] subclass.
 */
class ScanIpListFragment : Fragment() {

    lateinit var list: RecyclerView
    lateinit var progressBar: ProgressBar
    lateinit var fab: FloatingActionButton

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_scan_ip_list, container, false)

        list = view.findViewById(R.id.listIp)
        progressBar = view.findViewById(R.id.progressBar)
        fab = view.findViewById(R.id.fab)

        return view
    }

}// Required empty public constructor
